/*
 * Copyright (c) 2014 RuneCasters IT Solutions.
 *
 * This file is part of VolumeScheduler.
 *
 * VolumeScheduler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VolumeScheduler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VolumeScheduler.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.com.runecasters.volumescheduler.app;

import android.app.*;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import au.com.runecasters.volumescheduler.R;
import au.com.runecasters.volumescheduler.model.DatabaseHelper;
import au.com.runecasters.volumescheduler.model.SchedulerRule;

import java.util.Locale;

public class RuleActivity extends Activity implements ActionBar.TabListener {
    public static final int TRIGGER_RULE = 0;
    public static final int VOLUME_RULE = 1;
    private boolean mTriggerRuleReady = false;
    private boolean mVolumeRuleReady = false;
    private boolean mNewRule = false;
    private Button mButtonCreateRule;
    protected SchedulerRule mSchedulerRule;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule);
        mButtonCreateRule = (Button) findViewById(R.id.buttonCreateRule);

        mSchedulerRule = getIntent().getParcelableExtra("existingRule");
        if (mSchedulerRule == null) {
            mSchedulerRule = new SchedulerRule(getIntent().getIntExtra("selector", -1));
            mNewRule = true;
        }

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        mButtonCreateRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
                    ScheduleRuleSetter ruleFragment = (ScheduleRuleSetter) mSectionsPagerAdapter.getItem(i);
                    ruleFragment.setRules(mSchedulerRule);
                }
                DatabaseHelper dbHelper = DatabaseHelper.getInstance(RuleActivity.this);
                if (mNewRule) {
                    dbHelper.addRule(mSchedulerRule);
                } else {
                    dbHelper.updateRule(mSchedulerRule);
                }
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public void ruleUpdate(int rule, boolean ready) {
        switch (rule) {
            case TRIGGER_RULE:
                mTriggerRuleReady = ready;
                break;
            case VOLUME_RULE:
                mVolumeRuleReady = ready;
                break;
        }
        if (mTriggerRuleReady && mVolumeRuleReady) {
            mButtonCreateRule.setEnabled(true);
        } else {
            mButtonCreateRule.setEnabled(false);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private Fragment[] mTabList;
        private String[] mTabLabels;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            Locale l = Locale.getDefault();
            int ruleType = mSchedulerRule.getRuleType();
            mTabList = new Fragment[2];
            mTabLabels = new String[2];
            if (ruleType == SchedulerRule.TIME_RULE) {
                mTabList[0] = new TimeRuleFragment();
                mTabLabels[0] = getString(R.string.tab_timeSelector).toUpperCase(l);
            } else if (ruleType == SchedulerRule.CALENDAR_RULE) {
                mTabList[0] = new CalendarRuleFragment();
                mTabLabels[0] = getString(R.string.tab_calendarSelector).toUpperCase(l);
            } else {
                throw new IllegalArgumentException("Selector int " + ruleType + " is invalid");
            }
            mTabList[1] = new VolumeRuleFragment();
            mTabLabels[1] = getString(R.string.tab_volumeOptions).toUpperCase(l);
        }

        @Override
        public Fragment getItem(int position) {
            return mTabList[position];
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabLabels[position];
        }
    }

    public interface ScheduleRuleSetter {
        public void setRules(SchedulerRule schedulerRule);
    }
}
